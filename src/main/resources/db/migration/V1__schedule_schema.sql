create table online_course_info
(
    id          bigserial primary key,
    online_link text not null
);

create table offline_course_info
(
    id                bigserial primary key,
    course_address    text not null,
    course_university text not null
);

create table mentor
(
    id   bigserial primary key,
    name text not null,
    age  int  not null,
    role text not null
);

create table course
(
    id                     bigserial primary key,
    name                   text   not null,
    description            text   not null,
    student_count          bigint not null,
    mentor_id              bigint not null,
    online_course_info_id  bigint,
    offline_course_info_id bigint,

    foreign key (mentor_id) references mentor (id),
    foreign key (online_course_info_id) references online_course_info (id),
    foreign key (offline_course_info_id) references offline_course_info (id)
);


create table lesson
(
    id          bigserial primary key,
    title       text      not null,
    description text      not null,
    mentor_id   bigint    not null,
    time        timestamp not null,
    course_id   bigint,

    foreign key (mentor_id) references mentor (id),
    foreign key (course_id) references course (id)
);

insert into mentor(id, name, age, role)
values (0, 'ADMIN', 0, 'ROLE_ADMIN');
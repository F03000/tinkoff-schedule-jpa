package ru.tinkoff.academy.tinkoffschedule.lesson;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.academy.tinkoffschedule.lesson.dto.CreatingLessonDto;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@Tag(name = "Lesson")
@RequestMapping("/lesson")
@RequiredArgsConstructor
public class LessonController {

    private final LessonService lessonService;

    @Operation(summary = "Find all lessons")
    @GetMapping
    public List<Lesson> findAll() {
        return lessonService.findAll();
    }

    @Operation(summary = "Get lesson by id")
    @GetMapping("/{lessonId}")
    public Lesson getById(@PathVariable Long lessonId) {
        return lessonService.getById(lessonId);
    }

    @Operation(summary = "Save lesson from DTO")
    @PostMapping
    public Lesson save(@Valid @RequestBody CreatingLessonDto lessonDto) {
        return lessonService.save(lessonDto);
    }

    @Operation(summary = "Replace lesson from DTO")
    @PutMapping("{lessonId}")
    public Lesson replaceLesson(@PathVariable Long lessonId, @Valid @RequestBody CreatingLessonDto lessonDto) {
        return lessonService.replaceLesson(lessonId, lessonDto);
    }

    @Operation(summary = "Delete lesson by id")
    @DeleteMapping("{lessonId}")
    public void deleteLesson(@PathVariable Long lessonId) {
        lessonService.deleteById(lessonId);
    }

    @Operation(summary = "Assign mentor to lesson")
    @PostMapping("{lessonId}/mentor/{mentorId}")
    public Lesson replaceMentorByLessonId(@PathVariable Long lessonId, @PathVariable Long mentorId) {
        return lessonService.replaceMentorByLessonId(lessonId, mentorId);
    }

    @Operation(summary = "Save lesson from DTO with mentorId")
    @PostMapping("/mentor/{mentorId}")
    public Lesson saveWithMentorId(@PathVariable Long mentorId, @Valid @RequestBody CreatingLessonDto lessonDto) {
        return lessonService.save(lessonDto, mentorId);
    }

    @Operation(summary = "Replace lesson from DTO")
    @PutMapping("{lessonId}/mentor/{mentorId}")
    public Lesson replaceLessonWithMentorId(@PathVariable Long lessonId, @PathVariable Long mentorId, @Valid @RequestBody CreatingLessonDto lessonDto) {
        return lessonService.replaceLesson(lessonId, lessonDto, mentorId);
    }

    @Operation(summary = "Delete lesson by id")
    @DeleteMapping("{lessonId}/mentor/{mentorId}")
    public void deleteLessonWithMentorId(@PathVariable Long lessonId, @PathVariable Long mentorId) {
        lessonService.deleteById(lessonId, mentorId);
    }

    @Operation(summary = "Get mentor's lessons schedule")
    @GetMapping("/{mentorId}/schedule")
    public List<Date> getScheduleByMentorId(@PathVariable Long mentorId) {
        return lessonService.getScheduleByMentorId(mentorId);
    }

}

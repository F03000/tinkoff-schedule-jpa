package ru.tinkoff.academy.tinkoffschedule.lesson;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkoffschedule.core.exception.AccessForbiddenException;
import ru.tinkoff.academy.tinkoffschedule.core.exception.EntityNotFoundException;
import ru.tinkoff.academy.tinkoffschedule.lesson.dto.CreatingLessonDto;
import ru.tinkoff.academy.tinkoffschedule.mentor.Mentor;
import ru.tinkoff.academy.tinkoffschedule.mentor.MentorService;
import ru.tinkoff.academy.tinkoffschedule.mentor.role.Role;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LessonService {

    private final LessonRepository lessonRepository;

    private final MentorService mentorService;

    public Lesson getById(Long lessonId) {
        return lessonRepository.findById(lessonId)
                .orElseThrow(() -> new EntityNotFoundException("Lesson didn't found by id=" + lessonId));
    }

    public List<Lesson> findAll() {
        return lessonRepository.findAll();
    }

    private Lesson buildLessonFromDTO(CreatingLessonDto lessonDto) {
        Mentor mentor = mentorService.getById(lessonDto.getMentorId());

        return Lesson.builder()
                .mentor(mentor)
                .description(lessonDto.getDescription())
                .title(lessonDto.getTitle())
                .time(lessonDto.getTime())
                .courseId(lessonDto.getCourseId())
                .build();
    }

    @Transactional
    public Lesson save(CreatingLessonDto lessonDto) {
        Lesson lesson = buildLessonFromDTO(lessonDto);

        return lessonRepository.save(lesson);
    }

    @Transactional
    public Lesson save(CreatingLessonDto lessonDto, Long mentorId) {
        validateMentor(lessonDto.getMentorId(), mentorId);
        return save(lessonDto);
    }

    /**
     * Checks if mentor with id has permission to change lesson
     *
     * @param lessonMentorId - mentor id of lesson
     * @param mentorId       - mentor id of someone wanting to change this lesson
     */
    private void validateMentor(Long lessonMentorId, Long mentorId) {
        Mentor mentor = mentorService.getById(mentorId);
        if (mentor.getRole() != Role.ROLE_ADMIN && !Objects.equals(mentorId, lessonMentorId)) {
            throw new AccessForbiddenException("Mentor does not match mentor from existing lesson");
        }
    }

    @Transactional
    public Lesson replaceLesson(Long lessonId, CreatingLessonDto lessonDto) {
        Optional<Lesson> existingLesson = lessonRepository.findById(lessonId);
        if (existingLesson.isEmpty()) {
            throw new EntityNotFoundException("There is no existing lesson with id = " + lessonId);
        }

        Lesson lesson = buildLessonFromDTO(lessonDto);
        lesson.setId(lessonId);

        return lessonRepository.save(lesson);
    }

    @Transactional
    public Lesson replaceLesson(Long lessonId, CreatingLessonDto lessonDto, Long mentorId) {
        validateMentor(lessonDto.getMentorId(), mentorId);
        return replaceLesson(lessonId, lessonDto);
    }


    public void deleteById(Long lessonId) {
        try {
            lessonRepository.deleteById(lessonId);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("There is no existing lesson with id = " + lessonId);
        }
    }

    @Transactional
    public void deleteById(Long lessonId, Long mentorId) {
        Lesson lesson = getById(lessonId);
        validateMentor(lesson.getMentor().getId(), mentorId);
        deleteById(lessonId);
    }

    @Transactional
    public Lesson replaceMentorByLessonId(Long lessonId, Long mentorId) {
        Mentor mentor = mentorService.getById(mentorId);
        Lesson lesson = getById(lessonId);

        lesson.setMentor(mentor);
        return lessonRepository.save(lesson);
    }

    /**
     * Get dates of scheduled lessons for mentor with id
     *
     * @return list of dates of lessons
     */
    @Transactional
    public List<Date> getScheduleByMentorId(Long mentorId) {
        return lessonRepository.findAll().stream()
                .filter(lesson -> Objects.equals(lesson.getMentor().getId(), mentorId))
                .map(Lesson::getTime)
                .collect(Collectors.toList());
    }

    /**
     * Copy and save all the lessons with mentor by id. New lessons will have new mentor, course and zero date
     *
     * @param courseId    - course all existing lessons belong to
     * @param newCourseId - new course all new lessons should belong to
     * @param newMentorId - new mentor id
     */
    @Transactional
    public void copyLessonsByCourseIdAndMentorId(Long courseId, Long newCourseId, Long newMentorId) {
        findAll().stream()
                .filter(lesson -> Objects.equals(lesson.getCourseId(), courseId))
                .map(lesson -> createNewLessonFromLessonAndCourseId(lesson, newCourseId, newMentorId))
                .forEach(lessonRepository::save);
    }

    /**
     * Creates new lesson from existing one with new course, mentor and zero date
     *
     * @param lesson   - existing lessong
     * @param courseId - new course id
     * @param mentorId - new mentor id
     * @return - new lesson
     */
    public Lesson createNewLessonFromLessonAndCourseId(Lesson lesson, Long courseId, Long mentorId) {
        CreatingLessonDto creatingLessonDto = CreatingLessonDto.builder()
                .courseId(courseId)
                .description(lesson.getDescription())
                .title(lesson.getTitle())
                .time(new Date(0))
                .mentorId(mentorId)
                .build();
        return buildLessonFromDTO(creatingLessonDto);
    }
}

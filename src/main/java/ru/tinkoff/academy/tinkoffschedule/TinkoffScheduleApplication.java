package ru.tinkoff.academy.tinkoffschedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TinkoffScheduleApplication {

    public static void main(String[] args) {
        SpringApplication.run(TinkoffScheduleApplication.class, args);
    }

}

package ru.tinkoff.academy.tinkoffschedule.onlinecourseinfo;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;


@Entity
@Table(name = "online_course_info")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OnlineCourseInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "online_link")
    private String onlineLink;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        OnlineCourseInfo courseInfo = (OnlineCourseInfo) o;
        return id != null && Objects.equals(id, courseInfo.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

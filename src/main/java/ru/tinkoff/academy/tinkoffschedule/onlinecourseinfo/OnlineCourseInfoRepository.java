package ru.tinkoff.academy.tinkoffschedule.onlinecourseinfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OnlineCourseInfoRepository extends JpaRepository<OnlineCourseInfo, Long> {

}

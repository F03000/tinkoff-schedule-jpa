package ru.tinkoff.academy.tinkoffschedule.onlinecourseinfo;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkoffschedule.core.exception.EntityNotFoundException;
import ru.tinkoff.academy.tinkoffschedule.onlinecourseinfo.dto.CreatingOnlineCourseInfoDto;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OnlineCourseInfoService {

    private final ModelMapper modelMapper;

    private final OnlineCourseInfoRepository courseInfoRepository;

    public OnlineCourseInfo getById(Long courseInfoId) {
        return courseInfoRepository.findById(courseInfoId)
                .orElseThrow(() -> new EntityNotFoundException("Online course info didn't found by id=" + courseInfoId));
    }

    public List<OnlineCourseInfo> findAll() {
        return courseInfoRepository.findAll();
    }

    private OnlineCourseInfo buildCourseInfoFromDTO(CreatingOnlineCourseInfoDto courseInfoDto) {
        return modelMapper.map(courseInfoDto, OnlineCourseInfo.class);
    }

    public OnlineCourseInfo save(CreatingOnlineCourseInfoDto courseInfoDto) {
        OnlineCourseInfo courseInfo = buildCourseInfoFromDTO(courseInfoDto);

        return courseInfoRepository.save(courseInfo);
    }

    @Transactional
    public OnlineCourseInfo replaceCourseInfo(Long courseInfoId, CreatingOnlineCourseInfoDto courseInfoDto) {
        Optional<OnlineCourseInfo> existingCourseInfo = courseInfoRepository.findById(courseInfoId);
        if (existingCourseInfo.isEmpty()) {
            throw new EntityNotFoundException("There is no existing online course info with id = " + courseInfoId);
        }

        OnlineCourseInfo courseInfo = buildCourseInfoFromDTO(courseInfoDto);
        courseInfo.setId(courseInfoId);

        return courseInfoRepository.save(courseInfo);
    }


    public void deleteById(Long courseInfoId) {
        try {
            courseInfoRepository.deleteById(courseInfoId);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("There is no existing online course info with id = " + courseInfoId);
        }
    }
}

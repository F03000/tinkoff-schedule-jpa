package ru.tinkoff.academy.tinkoffschedule.onlinecourseinfo;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.academy.tinkoffschedule.onlinecourseinfo.dto.CreatingOnlineCourseInfoDto;

import javax.validation.Valid;
import java.util.List;

@RestController
@Tag(name = "Information about online courses")
@RequestMapping("/courseInfo/online")
@RequiredArgsConstructor
public class OnlineCourseInfoController {

    private final OnlineCourseInfoService courseInfoService;

    @Operation(summary = "Find all information about online courses")
    @GetMapping
    public List<OnlineCourseInfo> findAll() {
        return courseInfoService.findAll();
    }

    @Operation(summary = "Get information about online course by id")
    @GetMapping("/{courseInfoId}")
    public OnlineCourseInfo getById(@PathVariable Long courseInfoId) {
        return courseInfoService.getById(courseInfoId);
    }

    @Operation(summary = "Save information about online course from DTO")
    @PostMapping
    public OnlineCourseInfo save(@Valid @RequestBody CreatingOnlineCourseInfoDto courseInfoDto) {
        return courseInfoService.save(courseInfoDto);
    }

    @Operation(summary = "Replace online course information from DTO")
    @PutMapping("{courseInfoId}")
    public OnlineCourseInfo replaceCourseInfo(@PathVariable Long courseInfoId, @Valid @RequestBody CreatingOnlineCourseInfoDto courseInfoDto) {
        return courseInfoService.replaceCourseInfo(courseInfoId, courseInfoDto);
    }

    @Operation(summary = "Delete online course information by id")
    @DeleteMapping("{courseInfoId}")
    public void deleteCourseInfo(@PathVariable Long courseInfoId) {
        courseInfoService.deleteById(courseInfoId);
    }

}

package ru.tinkoff.academy.tinkoffschedule.offlinecourseinfo;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "offline_course_info")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OfflineCourseInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "course_address", nullable = false)
    private String courseAddress;

    @Column(name = "course_university", nullable = false)
    private String courseUniversity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        OfflineCourseInfo courseInfo = (OfflineCourseInfo) o;
        return id != null && Objects.equals(id, courseInfo.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

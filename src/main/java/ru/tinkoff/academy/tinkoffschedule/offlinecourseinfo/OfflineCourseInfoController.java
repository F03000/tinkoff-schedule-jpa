package ru.tinkoff.academy.tinkoffschedule.offlinecourseinfo;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.academy.tinkoffschedule.offlinecourseinfo.dto.CreatingOfflineCourseInfoDto;

import javax.validation.Valid;
import java.util.List;

@RestController
@Tag(name = "Information about offline courses")
@RequestMapping("/courseInfo/offline")
@RequiredArgsConstructor
public class OfflineCourseInfoController {

    private final OfflineCourseInfoService courseInfoService;

    @Operation(summary = "Find all information about offline courses")
    @GetMapping
    public List<OfflineCourseInfo> findAll() {
        return courseInfoService.findAll();
    }

    @Operation(summary = "Get information about offline course by id")
    @GetMapping("/{courseInfoId}")
    public OfflineCourseInfo getById(@PathVariable Long courseInfoId) {
        return courseInfoService.getById(courseInfoId);
    }

    @Operation(summary = "Save information about offline course from DTO")
    @PostMapping
    public OfflineCourseInfo save(@Valid @RequestBody CreatingOfflineCourseInfoDto courseInfoDto) {
        return courseInfoService.save(courseInfoDto);
    }

    @Operation(summary = "Replace online course information from DTO")
    @PutMapping("{courseInfoId}")
    public OfflineCourseInfo replaceCourseInfo(@PathVariable Long courseInfoId, @Valid @RequestBody CreatingOfflineCourseInfoDto courseInfoDto) {
        return courseInfoService.replaceCourseInfo(courseInfoId, courseInfoDto);
    }

    @Operation(summary = "Delete offline course information by id")
    @DeleteMapping("{courseInfoId}")
    public void deleteCourseInfo(@PathVariable Long courseInfoId) {
        courseInfoService.deleteById(courseInfoId);
    }

}

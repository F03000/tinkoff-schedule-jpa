package ru.tinkoff.academy.tinkoffschedule.offlinecourseinfo;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkoffschedule.core.exception.EntityNotFoundException;
import ru.tinkoff.academy.tinkoffschedule.offlinecourseinfo.dto.CreatingOfflineCourseInfoDto;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OfflineCourseInfoService {

    private final ModelMapper modelMapper;

    private final OfflineCourseInfoRepository courseInfoRepository;

    public OfflineCourseInfo getById(Long courseInfoId) {
        return courseInfoRepository.findById(courseInfoId)
                .orElseThrow(() -> new EntityNotFoundException("Offline course info didn't found by id=" + courseInfoId));
    }

    public List<OfflineCourseInfo> findAll() {
        return courseInfoRepository.findAll();
    }

    private OfflineCourseInfo buildCourseInfoFromDTO(CreatingOfflineCourseInfoDto courseInfoDto) {
        return modelMapper.map(courseInfoDto, OfflineCourseInfo.class);
    }

    public OfflineCourseInfo save(CreatingOfflineCourseInfoDto courseInfoDto) {
        OfflineCourseInfo courseInfo = buildCourseInfoFromDTO(courseInfoDto);

        return courseInfoRepository.save(courseInfo);
    }

    @Transactional
    public OfflineCourseInfo replaceCourseInfo(Long courseInfoId, CreatingOfflineCourseInfoDto courseInfoDto) {
        Optional<OfflineCourseInfo> existingCourseInfo = courseInfoRepository.findById(courseInfoId);
        if (existingCourseInfo.isEmpty()) {
            throw new EntityNotFoundException("There is no existing offline course info with id = " + courseInfoId);
        }

        OfflineCourseInfo courseInfo = buildCourseInfoFromDTO(courseInfoDto);
        courseInfo.setId(courseInfoId);

        return courseInfoRepository.save(courseInfo);
    }


    public void deleteById(Long courseInfoId) {
        try {
            courseInfoRepository.deleteById(courseInfoId);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("There is no existing offline course info with id = " + courseInfoId);
        }
    }
}

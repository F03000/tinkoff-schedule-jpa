package ru.tinkoff.academy.tinkoffschedule.offlinecourseinfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfflineCourseInfoRepository extends JpaRepository<OfflineCourseInfo, Long> {

}

package ru.tinkoff.academy.tinkoffschedule.offlinecourseinfo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingOfflineCourseInfoDto {
    @NotNull
    private String courseAddress;

    @NotNull
    private String courseUniversity;
}

package ru.tinkoff.academy.tinkoffschedule.course;

import lombok.*;
import org.hibernate.Hibernate;
import ru.tinkoff.academy.tinkoffschedule.offlinecourseinfo.OfflineCourseInfo;
import ru.tinkoff.academy.tinkoffschedule.onlinecourseinfo.OnlineCourseInfo;
import ru.tinkoff.academy.tinkoffschedule.lesson.Lesson;
import ru.tinkoff.academy.tinkoffschedule.mentor.Mentor;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "course")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "student_count")
    private Long studentCount;

    @ManyToOne
    @JoinColumn(name = "mentor_id")
    private Mentor mentor;

    @ManyToOne
    @JoinColumn(name = "online_course_info_id")
    private OnlineCourseInfo onlineCourseInfo;

    @ManyToOne
    @JoinColumn(name = "offline_course_info_id")
    private OfflineCourseInfo offlineCourseInfo;

    @ToString.Exclude
    @OneToMany
    @JoinColumn(name = "course_id")
    private List<Lesson> lessons;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Course course = (Course) o;
        return id != null && Objects.equals(id, course.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

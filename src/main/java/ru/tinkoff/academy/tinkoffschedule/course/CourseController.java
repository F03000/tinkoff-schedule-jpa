package ru.tinkoff.academy.tinkoffschedule.course;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.academy.tinkoffschedule.course.dto.CreatingCourseDto;

import javax.validation.Valid;
import java.util.List;

@RestController
@Tag(name = "Course")
@RequestMapping("/course")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @Operation(summary = "Find all courses")
    @GetMapping
    public List<Course> findAll() {
        return courseService.findAll();
    }

    @Operation(summary = "Get course by id")
    @GetMapping("/{courseId}")
    public Course getById(@PathVariable Long courseId) {
        return courseService.getById(courseId);
    }

    @Operation(summary = "Save course from DTO")
    @PostMapping
    public Course save(@Valid @RequestBody CreatingCourseDto courseDto) {
        return courseService.save(courseDto);
    }

    @Operation(summary = "Replace course from DTO")
    @PutMapping("{courseId}")
    public Course replaceCourse(@PathVariable Long courseId, @Valid @RequestBody CreatingCourseDto courseDto) {
        return courseService.replaceCourse(courseId, courseDto);
    }

    @Operation(summary = "Delete course by id")
    @DeleteMapping("{courseId}")
    public void deleteCourse(@PathVariable Long courseId) {
        courseService.deleteById(courseId);
    }

    @Operation(summary = "Save course from DTO with mentorId")
    @PostMapping("/mentor/{mentorId}")
    public Course saveWithMentorId(@PathVariable Long mentorId, @Valid @RequestBody CreatingCourseDto courseDto) {
        return courseService.save(courseDto, mentorId);
    }

    @Operation(summary = "Replace course from DTO")
    @PutMapping("{courseId}/mentor/{mentorId}")
    public Course replaceCourseWithMentorId(@PathVariable Long courseId, @PathVariable Long mentorId, @Valid @RequestBody CreatingCourseDto courseDto) {
        return courseService.replaceCourse(courseId, courseDto, mentorId);
    }

    @Operation(summary = "Delete course by id")
    @DeleteMapping("{courseId}/mentor/{mentorId}")
    public void deleteCourseWithMentorId(@PathVariable Long courseId, @PathVariable Long mentorId) {
        courseService.deleteById(courseId, mentorId);
    }

    @Operation(summary = "Copy course by id with mentor id")
    @PostMapping("/copy/{courseId}/mentor/{mentorId}")
    public Course copyCourseByCourseIdWithMentorId(@PathVariable Long courseId, @PathVariable Long mentorId) {
        return courseService.copyCourseByCourseIdWithMentorId(courseId, mentorId);
    }


}

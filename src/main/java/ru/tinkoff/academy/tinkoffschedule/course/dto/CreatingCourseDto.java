package ru.tinkoff.academy.tinkoffschedule.course.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingCourseDto {
    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private Long studentCount;

    @NotNull
    private Long mentorId;

    private Long onlineCourseInfoId;

    private Long offlineCourseInfoId;
}

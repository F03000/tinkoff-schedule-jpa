package ru.tinkoff.academy.tinkoffschedule.course;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkoffschedule.core.exception.AccessForbiddenException;
import ru.tinkoff.academy.tinkoffschedule.core.exception.BadRequestException;
import ru.tinkoff.academy.tinkoffschedule.core.exception.EntityNotFoundException;
import ru.tinkoff.academy.tinkoffschedule.course.dto.CreatingCourseDto;
import ru.tinkoff.academy.tinkoffschedule.lesson.LessonService;
import ru.tinkoff.academy.tinkoffschedule.offlinecourseinfo.OfflineCourseInfo;
import ru.tinkoff.academy.tinkoffschedule.offlinecourseinfo.OfflineCourseInfoService;
import ru.tinkoff.academy.tinkoffschedule.onlinecourseinfo.OnlineCourseInfo;
import ru.tinkoff.academy.tinkoffschedule.onlinecourseinfo.OnlineCourseInfoService;
import ru.tinkoff.academy.tinkoffschedule.mentor.Mentor;
import ru.tinkoff.academy.tinkoffschedule.mentor.MentorService;
import ru.tinkoff.academy.tinkoffschedule.mentor.role.Role;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CourseService {

    private final CourseRepository courseRepository;

    private final OnlineCourseInfoService onlineCourseInfoService;
    private final OfflineCourseInfoService offlineCourseInfoService;
    private final MentorService mentorService;
    private final LessonService lessonService;

    public Course getById(Long courseId) {
        return courseRepository.findById(courseId)
                .orElseThrow(() -> new EntityNotFoundException("Course didn't found by id=" + courseId));
    }

    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    /**
     * Validate and build course from existing DTO. Only one course info id should be set
     *
     * @return course build with validation
     */
    private Course buildCourseFromDTO(CreatingCourseDto courseDto) {
        if (courseDto.getOfflineCourseInfoId() != null && courseDto.getOnlineCourseInfoId() != null) {
            throw new BadRequestException("Online course info and offline course info could not be set together");
        }

        Mentor mentor = mentorService.getById(courseDto.getMentorId());

        Course.CourseBuilder courseBuilder = Course.builder()
                .mentor(mentor)
                .description(courseDto.getDescription())
                .name(courseDto.getName())
                .studentCount(courseDto.getStudentCount());

        if (courseDto.getOnlineCourseInfoId() != null) {
            OnlineCourseInfo onlineCourseInfo = onlineCourseInfoService.getById(courseDto.getOnlineCourseInfoId());
            courseBuilder.onlineCourseInfo(onlineCourseInfo);
        } else if (courseDto.getOfflineCourseInfoId() != null) {
            OfflineCourseInfo offlineCourseInfo = offlineCourseInfoService.getById(courseDto.getOfflineCourseInfoId());
            courseBuilder.offlineCourseInfo(offlineCourseInfo);
        }

        return courseBuilder.build();
    }

    @Transactional
    public Course save(CreatingCourseDto courseDto) {
        Course course = buildCourseFromDTO(courseDto);
        return courseRepository.save(course);
    }

    @Transactional
    public Course save(CreatingCourseDto courseDto, Long mentorId) {
        validateMentor(courseDto.getMentorId(), mentorId);
        return save(courseDto);
    }

    /**
     * Checks if mentor with id has permission to change course
     *
     * @param courseMentorId - mentor id of course
     * @param mentorId       - mentor id of someone wanting to change this course
     */
    private void validateMentor(Long courseMentorId, Long mentorId) {
        Mentor mentor = mentorService.getById(mentorId);
        if (mentor.getRole() != Role.ROLE_ADMIN && !Objects.equals(mentorId, courseMentorId)) {
            throw new AccessForbiddenException("Mentor does not match mentor from DTO");
        }
    }

    @Transactional
    public Course replaceCourse(Long courseId, CreatingCourseDto courseDto) {
        Optional<Course> existingCourse = courseRepository.findById(courseId);
        if (existingCourse.isEmpty()) {
            throw new EntityNotFoundException("There is no existing course with id = " + courseId);
        }

        Course course = buildCourseFromDTO(courseDto);
        course.setId(courseId);

        return courseRepository.save(course);
    }

    @Transactional
    public Course replaceCourse(Long courseId, CreatingCourseDto courseDto, Long mentorId) {
        validateMentor(courseDto.getMentorId(), mentorId);
        return replaceCourse(courseId, courseDto);
    }


    public void deleteById(Long courseId) {
        try {
            courseRepository.deleteById(courseId);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("There is no existing course with id = " + courseId);
        }
    }

    public void deleteById(Long courseId, Long mentorId) {
        Course course = getById(courseId);
        validateMentor(course.getMentor().getId(), mentorId);
        deleteById(courseId);
    }

    /**
     * Create new course with parameters copied from existing course. Sets new mentor and zero date for a new course
     *
     * @param courseId - existing course id
     * @param mentorId - mentor id of someone who wants to copy the course
     * @return new course
     */
    @Transactional
    public Course copyCourseByCourseIdWithMentorId(Long courseId, Long mentorId) {
        Course course = getById(courseId);
        mentorService.getById(mentorId);

        CreatingCourseDto.CreatingCourseDtoBuilder creatingCourseDtoBuilder = CreatingCourseDto.builder()
                .description(course.getDescription())
                .studentCount(course.getStudentCount())
                .name(course.getName())
                .mentorId(mentorId);
        if (course.getOfflineCourseInfo() != null) {
            creatingCourseDtoBuilder.offlineCourseInfoId(course.getOfflineCourseInfo().getId());
        } else if (course.getOnlineCourseInfo() != null) {
            creatingCourseDtoBuilder.onlineCourseInfoId(course.getOnlineCourseInfo().getId());
        }

        CreatingCourseDto dto = creatingCourseDtoBuilder.build();
        Course newCourse = courseRepository.save(buildCourseFromDTO(dto));

        lessonService.copyLessonsByCourseIdAndMentorId(courseId, newCourse.getId(), mentorId);

        return getById(newCourse.getId());
    }
}

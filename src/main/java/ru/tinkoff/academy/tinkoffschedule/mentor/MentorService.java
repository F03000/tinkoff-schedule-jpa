package ru.tinkoff.academy.tinkoffschedule.mentor;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkoffschedule.core.exception.EntityNotFoundException;
import ru.tinkoff.academy.tinkoffschedule.mentor.dto.CreatingMentorDto;
import ru.tinkoff.academy.tinkoffschedule.mentor.role.Role;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MentorService {

    private final ModelMapper modelMapper;

    private final MentorRepository mentorRepository;

    private static final Role DEFAULT_ROLE = Role.ROLE_TEACHER;

    public Mentor getById(Long mentorId) {
        return mentorRepository.findById(mentorId)
                .orElseThrow(() -> new EntityNotFoundException("Mentor didn't found by id=" + mentorId));
    }

    public List<Mentor> findAll() {
        return mentorRepository.findAll();
    }

    private Mentor buildMentorFromDTO(CreatingMentorDto mentorDto) {
        Mentor mentor = modelMapper.map(mentorDto, Mentor.class);
        mentor.setRole(DEFAULT_ROLE);
        return mentor;
    }

    @Transactional
    public Mentor save(CreatingMentorDto mentorDto) {
        Mentor mentor = buildMentorFromDTO(mentorDto);

        return mentorRepository.save(mentor);
    }

    @Transactional
    public Mentor replaceMentor(Long mentorId, CreatingMentorDto mentorDto) {
        Optional<Mentor> existingMentor = mentorRepository.findById(mentorId);
        if (existingMentor.isEmpty()) {
            throw new EntityNotFoundException("There is no existing mentor with id = " + mentorId);
        }

        Mentor mentor = buildMentorFromDTO(mentorDto);
        mentor.setId(mentorId);

        return mentorRepository.save(mentor);
    }


    public void deleteById(Long mentorId) {
        try {
            mentorRepository.deleteById(mentorId);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("There is no existing mentor with id = " + mentorId);
        }
    }
}

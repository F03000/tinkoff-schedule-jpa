package ru.tinkoff.academy.tinkoffschedule.mentor.role;

public enum Role {
    ROLE_TEACHER, ROLE_ADMIN
}

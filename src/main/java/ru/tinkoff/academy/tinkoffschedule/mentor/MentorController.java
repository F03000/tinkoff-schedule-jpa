package ru.tinkoff.academy.tinkoffschedule.mentor;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.academy.tinkoffschedule.mentor.dto.CreatingMentorDto;

import javax.validation.Valid;
import java.util.List;

@RestController
@Tag(name = "Mentor")
@RequestMapping("/mentor")
@RequiredArgsConstructor
public class MentorController {

    private final MentorService mentorService;

    @GetMapping
    @Operation(summary = "Find all mentors")
    public List<Mentor> findAll() {
        return mentorService.findAll();
    }

    @Operation(summary = "Get mentor by id")
    @GetMapping("/{mentorId}")
    public Mentor getById(@PathVariable Long mentorId) {
        return mentorService.getById(mentorId);
    }

    @Operation(summary = "Save mentor from DTO")
    @PostMapping
    public Mentor save(@Valid  @RequestBody CreatingMentorDto mentorDto) {
        return mentorService.save(mentorDto);
    }

    @Operation(summary = "Replace mentor from DTO")
    @PutMapping("{mentorId}")
    public Mentor replaceMentor(@PathVariable Long mentorId, @Valid @RequestBody CreatingMentorDto mentorDto) {
        return mentorService.replaceMentor(mentorId, mentorDto);
    }

    @Operation(summary = "Delete mentor by id")
    @DeleteMapping("{mentorId}")
    public void deleteMentor(@PathVariable Long mentorId) {
        mentorService.deleteById(mentorId);
    }

}

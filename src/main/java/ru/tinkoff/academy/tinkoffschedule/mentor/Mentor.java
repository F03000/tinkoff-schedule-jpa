package ru.tinkoff.academy.tinkoffschedule.mentor;

import lombok.*;
import org.hibernate.Hibernate;
import ru.tinkoff.academy.tinkoffschedule.mentor.role.Role;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "mentor")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Mentor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private Integer age;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Mentor mentor = (Mentor) o;
        return id != null && Objects.equals(id, mentor.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

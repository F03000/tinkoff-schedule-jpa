package ru.tinkoff.academy.tinkoffschedule.mentor.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatingMentorDto {
    @NotNull
    private String name;

    @NotNull
    private Integer age;
}

package ru.tinkoff.academy.tinkoffschedule.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        info = @Info(
                title = "Course board",
                description = "Teacher planning board"),
        servers = @Server(url = "http://localhost:8080")
)
public class OpenAPIConfiguration {
}
package ru.tinkoff.academy.tinkoffschedule.core.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * Exception which is thrown when input does not match expectations
 */
@ResponseStatus(value = BAD_REQUEST)
public class BadRequestException extends RuntimeException {

    /**
     * Constructors kept for extensibility reasons
     */
    public BadRequestException() {
    }

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRequestException(Throwable cause) {
        super(cause);
    }

    public BadRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
